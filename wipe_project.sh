#! /bin/bash
set -euo pipefail
IFS=$'\n\t'

pushd "${PWD}"
cd myproject
env_path=$(poetry env info --path || true)
if [[ ${env_path} ]]
then
    poetry env remove $(poetry env info --path)/bin/python
fi
popd
rm -rf myproject
